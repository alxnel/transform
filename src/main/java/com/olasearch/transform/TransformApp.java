package com.olasearch.transform;



import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.olasearch.transform.Transform;

public class TransformApp {

	
	public static Map<String,String> configMap = new HashMap<String,String> ();
	
	public static void main(String[] args) throws Exception {
	  
		parseCommandLine(args);
		 
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
 
        Server jettyServer = new Server(8080);
        jettyServer.setHandler(context);
 
        ServletHolder jerseyServlet = context.addServlet(
             org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
 
        // Tells the Jersey Servlet which REST service/class to load.
        jerseyServlet.setInitParameter(
           "jersey.config.server.provider.classnames",
           Transform.class.getCanonicalName());
 
        try {
            jettyServer.start();
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
		
	}

private static void parseCommandLine(String[] args) {
		
				
				try  {
                
					configMap = Files.lines(Paths.get(args[0]))
					.map(s-> s.trim())
					.filter(s -> s.startsWith("#") == false)
					.collect(Collectors.toMap(k -> k.split(":")[0], v -> v.split(":")[1]));

				} catch (IOException e) {
					e.printStackTrace();
				}
				
	}
	
}
