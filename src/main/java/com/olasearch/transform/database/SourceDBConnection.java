package com.olasearch.transform.database;

import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;

public class SourceDBConnection {

	public static ArrayList<Document> getSourceMongoDBConnection(String host, int port ,String dbName,String collection) { 
		 
		ArrayList<Document> dbDocs = null;
		
		try {

		
		@SuppressWarnings("resource")
		MongoClient mongoSourceClient = new MongoClient(host, port);
		
		MongoDatabase db = mongoSourceClient.getDatabase(dbName);
        dbDocs = db.getCollection(collection).find().into(new ArrayList<Document> ()); 
        
			

		    } catch (MongoException e) {
			e.printStackTrace();
		    }
		
		
		return dbDocs;
	
		
	}
	

}
