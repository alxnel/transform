package com.olasearch.transform;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.bson.Document;
import org.json.JSONObject;

import com.olasearch.transform.commands.JsonCommandDecoder;
import com.olasearch.transform.database.DestinationDBConnection;
import com.olasearch.transform.database.SourceDBConnection;

import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;


@Path("transform")	
public class Transform {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt() {
		return "Got it!";
	}
	
	
	@POST
	@Consumes({ MediaType.TEXT_PLAIN })
	@Produces({ MediaType.APPLICATION_JSON })

	public void transform(String jsonCommand) throws ResourceInstantiationException, ExecutionException {
	
		List<List<Map<String,String>>> finalMap = new ArrayList<List<Map<String,String>>> ();
		List<Map<String,String>> dataMap = JsonToMapConverter.convertJsontoMap(SourceDBConnection.getSourceMongoDBConnection(TransformApp.configMap.get("Source_Hostname"),Integer.parseInt(TransformApp.configMap.get("Source_Port")),TransformApp.configMap.get("Source_DBName"),JsonCommandDecoder.getCollectionName(jsonCommand)));
        finalMap.add(dataMap);
		
		JsonCommandDecoder.parseJsonCommand(jsonCommand);
			
		for(int i = 0 ; i< JsonCommandDecoder.commandList.size(); i++) {

        if (JsonCommandDecoder.commandList.get(i) != null) {		
		List<Map<String,String>> tmpOutPutMap  = JsonCommandDecoder.commandList.get(i).execute(finalMap.get(i)); 
		finalMap.add(tmpOutPutMap);
        }
		}
		
			
		for(int k = 0 ; k < finalMap.size(); k++) {
			List<Map<String,String>> tmp = finalMap.get(k);
			for (int j = 0 ; j< tmp.size(); j++) {
				Map<String,String> tMap = tmp.get(j);
				JSONObject mapObj = new JSONObject(tMap);
				System.out.println(mapObj.toString());
//				tMap.forEach( (k1,v1) -> System.out.println("Key: " + k1 + ": Value: " + v1));
			}
			System.out.println("\n");
		} 
		
		int len = finalMap.size();
		List<Document> mongoDocs = new ArrayList<Document> ();
		List<Map<String,String>> outMap = finalMap.get(len-1);
		for (int k = 0; k < outMap.size();k++) {
			Map<String,String> tMap = outMap.get(k);
			JSONObject mapObject = new JSONObject(tMap);
			Document doc = Document.parse( mapObject.toString());
			mongoDocs.add(doc);
		}
	
		DestinationDBConnection.writeToDestinationDB(TransformApp.configMap.get("Dest_Hostname"),Integer.parseInt(TransformApp.configMap.get("Dest_Port")),TransformApp.configMap.get("Dest_DBName"),JsonCommandDecoder.getCollectionName(jsonCommand) ,mongoDocs);
		
	}
	
	
}
