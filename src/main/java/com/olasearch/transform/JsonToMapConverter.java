package com.olasearch.transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JsonToMapConverter {

	static List<Map<String,String>> dataMap = new ArrayList<Map<String,String>> ();
	
	public static List<Map<String,String>> convertJsontoMap(ArrayList<Document> mongoDocs) {
		
		for (Document document : mongoDocs) {
			 
		   String jsonDoc = document.toJson().toString();
		   JSONObject tmpObj = new JSONObject(jsonDoc);
		   tmpObj.remove("_id");
		   HashMap<String,String> jsonMap = new Gson().fromJson(tmpObj.toString(), new TypeToken<HashMap<String, String>>(){}.getType());
//		   jsonMap.forEach( (k,v) -> System.out.println("Key: " + k + ": Value: " + v));
		   dataMap.add(jsonMap);
		   
		}
		
		return dataMap;
	}
	
}
