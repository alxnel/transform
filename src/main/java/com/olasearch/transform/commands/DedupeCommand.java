package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;

public class DedupeCommand extends Command {

	static String fieldName;
	static List<String> otherFields;
	
	Map <Integer,Long> dmapHashes = null;
	
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		DedupeCommand.fieldName = fieldName;
	}

	public List<String> getOtherFields() {
		return otherFields;
	}

	public void setOtherFields(List<String> otherFields) {
		DedupeCommand.otherFields = otherFields;
	}

	public DedupeCommand() {
		super();
	}
    
	@Override
	public List<Map<String, String>> execute(List<Map<String, String>> dataList)
			throws ResourceInstantiationException, ExecutionException {
		
		
		List<Map<String, String>> datanew_map = new ArrayList<Map<String, String>>();
		otherFields.add(fieldName);
		dmapHashes = generateHashes(dataList, (ArrayList<String>) otherFields);
		System.out.println("Generated Hashes Succesfully  " + dmapHashes.size());
		List<Long> dmapHashesAlone = createListWithKeys(dmapHashes);
		Collection<Long> dmapHashCollection = dmapHashesAlone;
		boolean isdupepresent = hasDuplicate(dmapHashCollection);
		System.out.println(isdupepresent);
		List<Long> dupList = new ArrayList<Long> ();
		if (isdupepresent == true) {
		dupList = getDuplicate(dmapHashCollection);
		HashSet<Long> dupListHash = new HashSet<Long>(dupList);
		List<Long> newdupList = new ArrayList<Long> (dupListHash);
		List<List<Integer>> indexesToMerge = getDuplicateIndexes(newdupList);
		datanew_map =  mergeIndexes(indexesToMerge,dataList);
//		System.out.println("Indexes to merge  " + indexesToMerge.get(1));
		
		
		}     
	   		
			
			
			
			return datanew_map;
}

public long generateHash(String s) {
		
		return new HashCodeBuilder(17,37).append(s).toHashCode();
	}
	
	public Map<Integer,Long> generateHashes(List<Map<String,String>> data,ArrayList<String> coloumns) {
		Map<Integer,Long> generatedHashes = new HashMap<Integer,Long> ();
		System.out.println("No of data to process  " + data.size());
		for (int i = 0; i < data.size(); i++) {
			Map<String, String> toProcess = data.get(i);
			String joinString = "";
			long joinHash = 0;
			for (Map.Entry<String, String> entry : toProcess.entrySet()) {
				
			    
			    
				if (coloumns.contains(entry.getKey())) {
					joinString += entry.getValue();
					
				}
                		
				
			}
			System.out.println(joinString);
			joinHash = generateHash(joinString);
			generatedHashes.put(i,joinHash);
		}
		return generatedHashes;
		
	}
	
	public static <T> List<T> getDuplicate(Collection<T> list) {

	    final List<T> duplicatedObjects = new ArrayList<T>();
	    Set<T> set = new HashSet<T>() {
	    /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		@Override
	    public boolean add(T e) {
	        if (contains(e)) {
	            duplicatedObjects.add(e);
	        }
	        return super.add(e);
	    }
	    };
	   for (T t : list) {
	        set.add(t);
	    }
	    return duplicatedObjects;
	}


	public static <T> boolean hasDuplicate(Collection<T> list) {
	    if (getDuplicate(list).isEmpty())
	        return false;
	    return true;
	}

	public List<Long> createListWithKeys(Map<Integer, Long> dmapHashes) {
		List<Long> listHashes = new ArrayList<Long> ();
		for (Entry<Integer, Long> entry : dmapHashes.entrySet()) {
	     listHashes.add(entry.getValue());
		}
		return listHashes;
	}
	

	public List<List<Integer>> getDuplicateIndexes(List<Long> duplicates) {
		List<List<Integer>> dupIndexes = new ArrayList<List<Integer>> ();
		for (Long dup : duplicates) {
			List<Integer> tmpList = new ArrayList<Integer> ();
			for (Map.Entry<Integer, Long> entry : dmapHashes.entrySet()) {
				System.out.println("Printing values " + entry.getValue() + "dup  " + dup);
				if (entry.getValue().compareTo(dup) == 0) {
					tmpList.add(entry.getKey());
				}
			}
		dupIndexes.add(tmpList);
		}
		
		return dupIndexes;
	}
	
/*	public  void mergeIndexes(List<List<Integer>> mergeIndexs) {
		
		
		
		for (int i =0; i < mergeIndexs.size(); i ++) {
			
			List<Integer> indexToMerge = mergeIndexs.get(i);
	
			System.out.println("Merging Indexes" + indexToMerge);
			Map<String,String> tmpMap = mergeOriginalDataMaps(indexToMerge);
			System.out.println("Cretaed tmp merged map");
			csvdatanew_map.add(tmpMap);
			
			for (int index : indexToMerge) {
			System.out.println("Now I am " + csvdatanew_map.size()+ " Long");	
			csvdatanew_map.remove(index);
			System.out.println("Removed index " + index);
				
			}
			
				
		}
		
		
		
		
		
		
		
	}
	*/
	
	
	public Map<String,String> mergeOriginalDataMaps(List<Integer> indexList, List<Map<String,String>> originalData) {
		
		List<Map<String,String>> tmpMap = new ArrayList<Map<String,String>> ();
		
		for (int index : indexList ) {
			
			tmpMap.add(originalData.get(index));
		}
	
		Map<String, String> result =
			    tmpMap.stream()
			        .flatMap(m -> m.entrySet().stream())
			        .collect(Collectors.groupingBy(
			            Map.Entry::getKey,
			            Collectors.mapping(
			                Map.Entry::getValue,
			                Collectors.collectingAndThen(Collectors.<String>toSet(), s -> String.join(",", s))
			            )
			        ));
		
		
		return result;
		
		
	}
/*		Map<String, String> result =
			    tmpMap.stream()
			        .flatMap(m -> m.entrySet().stream())
			        .collect(Collectors.groupingBy(
			            Map.Entry::getKey,
			            Collectors.mapping(Map.Entry::getValue, distinct(Collectors.joining()))
			        ));
		
		 <T,A,R> Collector<T, ?, R> distinct(Collector<T, A, R> downstream) {
		    class Pair<U,V> {
		        U u; V v;
		        Pair(U u, V v) {
		            this.u = u;
		            this.v = v;
		        }
		    }
		    return Collector.of(
		            () -> new Pair<>(new HashSet<>(), downstream.supplier().get()), 
		            (p, e) -> {
		                if (!p.u.contains(e)) {
		                    downstream.accumulator().accept(p.v, e);
		                    p.u.add(e);
		                }
		            },
		            (p1, p2) -> {
		                p1.u.addAll(p2.u);
		                p1.v = downstream.combiner().apply(p1.v, p2.v);
		                return p1;
		            },
		            p -> downstream.finisher().apply(p.v)
		           );
		}
		
		
		
	}
	
*/	
	
	
	
	
	public List<Map<String,String>> mergeIndexes(List<List<Integer>> mergeIndexs,List<Map<String, String>> originalData ) {
		
	List<Integer> tmpMerged = new ArrayList<Integer> ();
	List<Map<String,String>> tmpfinalData = new ArrayList<Map<String,String>> ();
	
	for (int i = 0; i< mergeIndexs.size();i++) {
	
		tmpMerged.addAll(mergeIndexs.get(i));
	}
   
	for (int j=0; j< originalData.size(); j++) {
		if (tmpMerged.contains(j) == false) {
			tmpfinalData.add(originalData.get(j));
		}
	}
	
	System.out.println("New map contains " + tmpfinalData.size());
	for (int i =0; i < mergeIndexs.size(); i ++) {
		
		List<Integer> indexToMerge = mergeIndexs.get(i);

		System.out.println("Merging Indexes" + indexToMerge);
		Map<String,String> tmpMap = mergeOriginalDataMaps(indexToMerge, originalData);
		System.out.println("Cretaed tmp merged map");
	    tmpfinalData.add(tmpMap);
	
	} 
	    
	     System.out.println("Size of final tmp Map after merging " + tmpfinalData.size() );
	     return tmpfinalData;
	}
	
	
	
	
	
	
	
	
	
	
	
}
