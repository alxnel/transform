package com.olasearch.transform.commands;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.olasearch.transform.TransformApp;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.Gate;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.util.GateException;
import gate.util.GateRuntimeException;
import gate.util.InvalidOffsetException;
import gate.util.persistence.PersistenceManager;

public class EntityExtractionCommand extends Command {

	String fieldName;
	String new_Field_Name;
	String entityName;
	
	
	public String getFieldName() {
		return fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getNew_Field_Name() {
		return new_Field_Name;
	}


	public void setNew_Field_Name(String new_Field_Name) {
		this.new_Field_Name = new_Field_Name;
	}


	public String getEntityName() {
		return entityName;
	}


	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}


	public EntityExtractionCommand() {
		super();
	}


		@Override
	public String toString() {
		return "EntityExtractionCommand [fieldName=" + fieldName + ", new_Field_Name=" + new_Field_Name
				+ ", entityName=" + entityName + "]";
	}


		public List<Map<String, String>> execute(List<Map<String, String>> dataList) throws ResourceInstantiationException, ExecutionException {
		List<Map<String, String>> datanew_map = new ArrayList<Map<String, String>>();
		CorpusController annieController = null;
		try {
			annieController = createGateAnnieAnnotator();
			System.out.println("ANNIE Created Successfully");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
       for (int i = 0; i < dataList.size(); i++) {
		
			Map<String, String> tmpMap = new HashMap<String, String>();
			Map<String, String> toProcess = dataList.get(i);
		
            
			for (Map.Entry<String, String> entry : toProcess.entrySet()) {
				if (fieldName.contains(entry.getKey())) {
					tmpMap.put(entry.getKey(), entry.getValue());
					Corpus docCorpus = createGateDocument(entry.getValue());
					annieController.setCorpus(docCorpus);
					annieController.execute();
					ArrayList<String> personsFound = new ArrayList<String>();
					Iterator<Document> iter = docCorpus.iterator();
					while (iter.hasNext()) {
						Document doc = (Document) iter.next();
						AnnotationSet defaultAnnotSet = doc.getAnnotations();
						AnnotationSet personAnnoatation = defaultAnnotSet.get(entityName);

						for (Annotation personAnn : personAnnoatation) {
							try {
								String personStr = doc.getContent().getContent(personAnn.getStartNode().getOffset(),
										personAnn.getEndNode().getOffset()).toString();
								personsFound.add(personStr);
							} catch (InvalidOffsetException e) {
								throw new GateRuntimeException(e);
							}
						}
					}
					String listString = "";

					for (String s : personsFound) {
						listString += s + ",";
					}
					tmpMap.put(new_Field_Name, listString);
				} else {
					tmpMap.put(entry.getKey(), entry.getValue());
				}

			}

			datanew_map.add(tmpMap);

		}
		
		return datanew_map;

		
	}

	public void setGateProperties() {
		Properties props = System.getProperties();
		props.setProperty("gate.home", TransformApp.configMap.get("gate.home"));
		props.setProperty("gate.plugins.home", TransformApp.configMap.get("gate.plugins.home"));
		props.setProperty("gappfile", TransformApp.configMap.get("gappfile"));
		
	}
	
	private boolean gateInited = false;
	public CorpusController createGateAnnieAnnotator() throws GateException, IOException, URISyntaxException {
		if (gateInited == false) {
		    setGateProperties();
			Gate.init();
			gateInited = true;
		}
		CorpusController controller = (CorpusController) PersistenceManager
				.loadObjectFromFile(new File(System.getProperty("gappfile")));
		return controller;

	}

	public static Corpus createGateDocument(String text) throws ResourceInstantiationException {
		Document doc = Factory.newDocument(text);
		Corpus corpus = Factory.newCorpus("Corpus");
		corpus.add(doc);
		return corpus;
	}
}
