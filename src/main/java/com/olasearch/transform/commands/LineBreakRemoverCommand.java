package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;

public class LineBreakRemoverCommand extends Command {

	String fieldName;
	
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public List<Map<String, String>> execute(List<Map<String, String>> dataList)
			throws ResourceInstantiationException, ExecutionException {
		List<Map<String, String>> datanew_map = new ArrayList<Map<String, String>>();
		
		for (int i = 0; i < dataList.size(); i++) {
			Map<String, String> tmpMap = new HashMap<String, String>();

			Map<String, String> toProcess = dataList.get(i);
			for (Map.Entry<String, String> entry : toProcess.entrySet()) {
				if (fieldName.contains(entry.getKey())) {
				tmpMap.put(entry.getKey(),collapseWhiteSpace(entry.getValue())); }
				else {
				tmpMap.put(entry.getKey(), entry.getValue()); }
				
			}

			datanew_map.add(tmpMap);

		}
		return datanew_map;

	}

public static String collapseWhiteSpace(String text) {
		
		String removed_text = text.replace("\n", " ").replace("\r", " ").replace("\r\n", " ");
		return (removed_text);
	}
	
	
}
