package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;

public class JoinFieldCommand extends Command {

	static String fieldName;
	static String seperator;
	static List<String> otherFields;
	static String new_field_name;
	
	public String getNew_field_name() {
		return new_field_name;
	}

	public void setNew_field_name(String new_field_name) {
		JoinFieldCommand.new_field_name = new_field_name;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		JoinFieldCommand.fieldName = fieldName;
	}

	public String getSeperator() {
		return seperator;
	}

	public void setSeperator(String seperator) {
		JoinFieldCommand.seperator = seperator;
	}

	public List<String> getOtherFields() {
		return otherFields;
	}

	public void setOtherFields(List<String> otherFields) {
		JoinFieldCommand.otherFields = otherFields;
	}
    
	public JoinFieldCommand() {
		super();
	}

	@Override
	public List<Map<String, String>> execute(List<Map<String, String>> dataList)
			throws ResourceInstantiationException, ExecutionException {
		
		List<Map<String, String>> datanew_map = new ArrayList<Map<String, String>>();
		otherFields.add(fieldName);
		for (int i = 0; i < dataList.size(); i++) {
			Map<String, String> tmpMap = new HashMap<String, String>();
			ArrayList<String> tmpList = new ArrayList<String>();
			Map<String, String> toProcess = dataList.get(i);
			for (Map.Entry<String, String> entry : toProcess.entrySet()) {
				

				if (otherFields.contains(entry.getKey())) {
					tmpList.add(entry.getValue());
					tmpMap.put(entry.getKey(), entry.getValue());

				} else {
					tmpMap.put(entry.getKey(), entry.getValue());
				}
				

			}
			tmpMap.put(new_field_name, joinFieldValues(tmpList));
			datanew_map.add(tmpMap);

		}
		
		
		
		
		
		return datanew_map;
	}

	
public static String joinFieldValues(ArrayList<String> valuesToJoin) {
		
		String joinValue = "";
		for (String val : valuesToJoin) {
			joinValue += val + seperator;
		}
		return joinValue;
	}

}
