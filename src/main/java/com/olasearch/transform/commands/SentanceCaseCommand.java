package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;

public class SentanceCaseCommand extends Command {

	static String fieldName;
	static List<String> protectedWords = new ArrayList<String> ();
	
		public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		TitleCaseCommand.fieldName = fieldName;
	}

	public List<String> getProtectedWords() {
		return protectedWords;
	}


	public void setProtectedWords(List<String> protectedWords) {
		TitleCaseCommand.protectedWords = protectedWords;
	}


	

	
	
	
	@Override
	public List<Map<String, String>> execute(List<Map<String, String>> dataList)
			throws ResourceInstantiationException, ExecutionException {
List<Map<String, String>> datanew_map = new ArrayList<Map<String, String>>();
	  	
		for (int i = 0; i < dataList.size(); i++) {
			Map<String, String> tmpMap = new HashMap<String, String>();

			Map<String, String> toProcess = dataList.get(i);

			for (Map.Entry<String, String> entry : toProcess.entrySet()) {
				if (fieldName.contains(entry.getKey())) {
					String string_to_change;
					if (protectedWords.contains(entry.getValue())) {
						string_to_change = entry.getValue();
					} else {
						string_to_change = changeCasing(entry.getValue());
					}
					tmpMap.put(entry.getKey(), string_to_change);
				} else {
					tmpMap.put(entry.getKey(), entry.getValue());

				}

			}

			datanew_map.add(tmpMap);

		}

		return datanew_map;
	}

	
public static String changeCasing(String t) {
		
		StringBuffer buffy = new StringBuffer(t.toLowerCase()); 
		Pattern pattern = Pattern.compile("(^|\\.)\\s*(\\w)");
		Matcher matcher = pattern.matcher(buffy);
		while (matcher.find())
		    buffy.replace(matcher.end() - 1, matcher.end(), matcher.group(2).toUpperCase());
		return buffy.toString();
	
		
	}
	
}
