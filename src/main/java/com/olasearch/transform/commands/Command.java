package com.olasearch.transform.commands;

import java.util.List;
import java.util.Map;

import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;

public abstract class Command {

	
	public abstract List<Map<String, String>> execute( List<Map<String, String>> dataList) throws ResourceInstantiationException, ExecutionException;
}
