package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrimCommand extends Command{

	String fieldName;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public TrimCommand(String fieldName) {
		super();
		this.fieldName = fieldName;
	}

	public TrimCommand() {
		super();
	}
	
	public List<Map<String, String>> execute( List<Map<String, String>> dataList) {
		List<Map<String, String>> datanew_map = new ArrayList<Map<String, String>>();	
		for (int i = 0; i < dataList.size(); i++) {
			Map<String, String> tmpMap = new HashMap<String, String>();

			Map<String, String> toProcess = dataList.get(i);
			for (Map.Entry<String, String> entry : toProcess.entrySet()) {
				if (fieldName.contains(entry.getKey())) {
				tmpMap.put(entry.getKey().trim(), entry.getValue().trim()); }
				else {
				tmpMap.put(entry.getKey(), entry.getValue()); }
				
			}

			datanew_map.add(tmpMap);

		}
		return datanew_map;
		
	}
	
}
