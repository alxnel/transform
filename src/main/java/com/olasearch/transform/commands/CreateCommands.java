package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class CreateCommands {


	public static Command createCommands(String fieldName ,JSONObject moduleObject) {
		Command command = null;
		
		String moduleName = moduleObject.get("module").toString();
		System.out.println(moduleObject.toString());
	
		if (moduleName.contains("title-case")) {
			JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
			List<String> protected_words = new ArrayList<String>();
			JSONArray optionArray = new JSONArray(optionObject.getJSONArray("protected_words").toString());
			for (int k = 0; k < optionArray.length(); k++) {
				protected_words.add(optionArray.getString(k));
			}
			TitleCaseCommand titleCaseCommand = new TitleCaseCommand();
			titleCaseCommand.setFieldName(fieldName);
			titleCaseCommand.setProtectedWords(protected_words);
			command = titleCaseCommand;
			System.out.println("Created Title Case Command");
		}
		else if (moduleName.contains("trim")) {
			TrimCommand trimCommand = new TrimCommand();
			trimCommand.setFieldName(fieldName);
			command = trimCommand;
			System.out.println("Created Trim Command");
		}
		else if (moduleName.contains("collapse-whitespace")) {
		
			CollapseWhiteSpaceCommand collapseWhiteSpaceCommand = new CollapseWhiteSpaceCommand();
			collapseWhiteSpaceCommand.setFieldName(fieldName);
			command = collapseWhiteSpaceCommand;
			System.out.println("Created Collapse White Space Command");
		}
		
		else if (moduleName.contains("acronym-expander")) {
			AcronymExpansionCommand acronymExpansionCommand = new AcronymExpansionCommand();
			acronymExpansionCommand.setFieldName(fieldName);
		JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
			System.out.println("Created option object for acronym expansion");
			Map<String,String> acros = new HashMap<String,String> ();
			String new_field_name = optionObject.getString("new_field_name").toString();
			JSONArray acroArray = optionObject.getJSONArray("acros");
			for (Object element : acroArray) {
				String[] tmp = element.toString().split(",");
				acros.put(tmp[0].replaceAll("[^a-zA-Z ]", ""),tmp[1].replaceAll("[^a-zA-Z ]", ""));
			}
		    acronymExpansionCommand.setNew_Field_Name(new_field_name);
            acronymExpansionCommand.setAcros(acros);
            command = acronymExpansionCommand;  
            System.out.println("Created Acronym expansion Command");
		}
		
		else if (moduleName.contains("extract-names")) {
		EntityExtractionCommand entityExtractionCommand = new EntityExtractionCommand();
		entityExtractionCommand.setFieldName(fieldName);
		entityExtractionCommand.setEntityName("Person");
		JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
		String new_field_name = optionObject.getString("new_field_name").toString();
		entityExtractionCommand.setNew_Field_Name(new_field_name);;
        command = entityExtractionCommand;
        System.out.println("Created Entity Extraction Command");
		
		}

		else if (moduleName.contains("extract-location")) {
			EntityExtractionCommand entityExtractionCommand = new EntityExtractionCommand();
			entityExtractionCommand.setFieldName(fieldName);
			entityExtractionCommand.setEntityName("Location");
			JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
			String new_field_name = optionObject.getString("new_field_name").toString();
			entityExtractionCommand.setNew_Field_Name(new_field_name);;
	        command = entityExtractionCommand;
	        System.out.println("Created Entity Extraction Command");
			
			}
		
		else if (moduleName.contains("lower-case")) {
			JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
			List<String> protected_words = new ArrayList<String>();
			JSONArray optionArray = new JSONArray(optionObject.getJSONArray("protected_words").toString());
			for (int k = 0; k < optionArray.length(); k++) {
				protected_words.add(optionArray.getString(k));
			}
		   LowerCaseCommand lowerCaseCommand = new LowerCaseCommand();
		   lowerCaseCommand.setFieldName(fieldName);
		   lowerCaseCommand.setProtectedWords(protected_words);
		   command = lowerCaseCommand;
		
		}
		
		else if (moduleName.contains("upper-case")) {
			JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
			List<String> protected_words = new ArrayList<String>();
			JSONArray optionArray = new JSONArray(optionObject.getJSONArray("protected_words").toString());
			for (int k = 0; k < optionArray.length(); k++) {
				protected_words.add(optionArray.getString(k));
			}
		
			UpperCaseCommand upperCaseCommand = new UpperCaseCommand();
			upperCaseCommand.setFieldName(fieldName);
			upperCaseCommand.setProtectedWords(protected_words);
			command = upperCaseCommand;
			
		}
		
		else if (moduleName.contains("sentance-case")) {
			JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
			List<String> protected_words = new ArrayList<String>();
			JSONArray optionArray = new JSONArray(optionObject.getJSONArray("protected_words").toString());
			for (int k = 0; k < optionArray.length(); k++) {
				protected_words.add(optionArray.getString(k));
			}
		
			SentanceCaseCommand sentanceCaseCommand = new SentanceCaseCommand();
			sentanceCaseCommand.setFieldName(fieldName);
			sentanceCaseCommand.setProtectedWords(protected_words);
			command = sentanceCaseCommand;
			
		}
		
		else if (moduleName.contains("remove-linebreaks")) {
			LineBreakRemoverCommand lineBreakRemoverCommand = new LineBreakRemoverCommand();
			lineBreakRemoverCommand.setFieldName(fieldName);
			command = lineBreakRemoverCommand;
			
		}
		
		else if (moduleName.contains("join-fields")) {
		JoinFieldCommand joinFieldCommand = new JoinFieldCommand();
		joinFieldCommand.setFieldName(fieldName);
		JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
		String new_field_name = optionObject.getString("new_field_name").toString();
		String seperator = optionObject.getString("separator").toString();
		List<String> otherFields = new ArrayList<String>();
		JSONArray optionArray = new JSONArray(optionObject.getJSONArray("fields").toString());
		for (int k = 0; k < optionArray.length(); k++) {
			otherFields.add(optionArray.getString(k));
		}
		joinFieldCommand.setNew_field_name(new_field_name);
		joinFieldCommand.setSeperator(seperator);
		joinFieldCommand.setOtherFields(otherFields);
		command = joinFieldCommand;
		
		}
		
		else if (moduleName.contains("dedupe")) {
		
		DedupeCommand dedupeCommand = new DedupeCommand();
		dedupeCommand.setFieldName(fieldName);
		JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
		List<String> otherFields = new ArrayList<String>();
		JSONArray optionArray = new JSONArray(optionObject.getJSONArray("fields").toString());
		for (int k = 0; k < optionArray.length(); k++) {
			otherFields.add(optionArray.getString(k));
		}
		
		dedupeCommand.setOtherFields(otherFields);
		command = dedupeCommand;
		
		}
		
		else if (moduleName.contains("regex")) {
		RegexCommand regexCommand = new RegexCommand();
		regexCommand.setFieldName(fieldName);
		JSONObject optionObject = new JSONObject(moduleObject.opt("options").toString());
		List<String> regexArray = new ArrayList<String>();
		JSONArray optionArray = new JSONArray(optionObject.getJSONArray("regexToMatch").toString());	
		for (int k = 0; k < optionArray.length(); k++) {
			regexArray.add(optionArray.getJSONArray(k).toString());
		}	
		regexCommand.setRegexArray(regexArray);	
		
		command = regexCommand;
		
		}
		
		
		return command;
	}
	
}
