package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class JsonCommandDecoder {

	public static List<Command> commandList = new ArrayList<Command>();

	public static void parseJsonCommand(String jsonCommand) {
		// System.out.println(jsonCommand);
		JSONObject commandObject = new JSONObject(jsonCommand);
		
		JSONArray fieldRuleArray = commandObject.getJSONArray("field_rules");
		// System.out.println(fieldRuleArray.length());
		for (int i = 0; i < fieldRuleArray.length(); i++) {
			String fieldName = fieldRuleArray.getJSONObject(i).keys().next();
			System.out.println("Printing field name " + fieldName);
			Object object = fieldRuleArray.getJSONObject(i).get(fieldName);
			JSONArray moduleArray = new JSONArray(object.toString());
//			System.out.println(moduleArray.toString());
			for (int j = 0; j < moduleArray.length(); j++) {
				JSONObject moduleObject = new JSONObject(moduleArray.get(j).toString());
				commandList.add(CreateCommands.createCommands(fieldName, moduleObject));
			}
		}
	}
	
	public static String getCollectionName(String jsonCommand) {
		JSONObject commandObject = new JSONObject(jsonCommand);
		String collection = commandObject.getString("collection");
		return collection;
	}
}
