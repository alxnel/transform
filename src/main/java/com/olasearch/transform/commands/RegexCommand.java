package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;

import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;

public class RegexCommand extends Command {

	static String fieldName;
	static List<String> regexArray ;
	
	public RegexCommand() {
		super();
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		RegexCommand.fieldName = fieldName;
	}

	public List<String> getRegexArray() {
		return regexArray;
	}

	public void setRegexArray(List<String> regexArray) {
		RegexCommand.regexArray = regexArray;
	}

	@Override
	public List<Map<String, String>> execute(List<Map<String, String>> dataList)
			throws ResourceInstantiationException, ExecutionException {
		List<Map<String, String>> datanew_map = new ArrayList<Map<String, String>>();
		Map<Pattern, Map<String, String>> patterns_ToMatch = generatePatterns(regexArray);
		for (int i = 0; i < dataList.size(); i++) {
			Map<String, String> tmpMap = new HashMap<String, String>();

			Map<String, String> toProcess = dataList.get(i);
			for (Map.Entry<String, String> entry : toProcess.entrySet()) {
				if (fieldName.contains(entry.getKey())) {
					Pattern pattern = null;
					String replacement = null;
					String mode = null;
					String tmpVal = null;
					String matchString = entry.getValue();
					for (Map.Entry<Pattern, Map<String, String>> m1 : patterns_ToMatch.entrySet()) {
						pattern = m1.getKey();
						 
						for (Map.Entry<String, String> m2 : m1.getValue().entrySet()) {
							replacement = m2.getKey();

							mode = m2.getValue();
						}

						
						
						java.util.regex.Matcher matcher = pattern.matcher(matchString);
						
							if (matcher.find() == true) {
								if (mode.contains("first")) {
									tmpVal = matcher.replaceFirst(replacement);
								    matchString = tmpVal; }
								else if (mode.contains("all")) {
									tmpVal = matcher.replaceAll(replacement);
									matchString = tmpVal ; }  
								tmpMap.put(entry.getKey(), tmpVal);
							} else {
								tmpMap.put(entry.getKey(), entry.getValue());
							}
						
					}

				} else
					tmpMap.put(entry.getKey(), entry.getValue());

			}
			datanew_map.add(tmpMap);

		}
		return datanew_map;

	}

	
	
	public int createFlags(List<String> flagsGiven) {
		int flags = 0;
		String tmpflag = "";
		for (String s : flagsGiven) {

			tmpflag = tmpflag + s;
		}

		
		if (tmpflag.contains("i"))
			flags = Pattern.CASE_INSENSITIVE;
		else if (tmpflag.contains("m"))
			flags = Pattern.MULTILINE;
		else if (tmpflag.contains("im"))
			flags = Pattern.CASE_INSENSITIVE | Pattern.MULTILINE;
		else if (tmpflag.contains("mi"))
			flags = Pattern.MULTILINE | Pattern.CASE_INSENSITIVE;
		return flags;
	}

	public Map<Pattern, Map<String, String>> generatePatterns(List<String> rawPatternArray) {

		Map<Pattern, Map<String, String>> patternMap = new HashMap<Pattern, Map<String, String>>();

		for (String rawpattern : rawPatternArray) {
			
			List<String> flagsGiven = new ArrayList<String>();
			System.out.println(rawpattern);
			String patternString =  "^\\[\"/(.*)/(.*)\".*,.*\"(.*)\"\\]";
			Pattern pattern = Pattern.compile(patternString);
		    Matcher matcher = pattern.matcher(rawpattern);
			System.out.println(matcher.find());
			String patternToMatch = matcher.group(1);
			String flagsCombined = matcher.group(2);
			String replacement = matcher.group(3);
			
			System.out.println("Pattern " + patternToMatch + " Flags " + flagsCombined + " Replacement " + replacement );
			String mode = null;
			String[] flagSplit = flagsCombined.split("");
			if (ArrayUtils.contains(flagSplit, "g")) {
				mode = "all";
			}
			else {
				mode = "first";
			}
			
			
			for(int i = 0; i< flagSplit.length; i++) {
				if (flagSplit[i].contains("g") == false ) {
					flagsGiven.add(flagSplit[i]);
				}
			}
			int flags = createFlags(flagsGiven);
			
			Pattern p = Pattern.compile(patternToMatch, flags);
			Map<String, String> tmpMap = new HashMap<String, String>();
			tmpMap.put(replacement, mode);
			patternMap.put(p, tmpMap);
		}
		return patternMap;

	}

}
