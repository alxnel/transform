package com.olasearch.transform.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AcronymExpansionCommand extends Command {

	String fieldName;
	Map<String,String> acros = new HashMap<String,String> ();
	String new_Field_Name;
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Map<String, String> getAcros() {
		return acros;
	}
	public void setAcros(Map<String, String> acros) {
		this.acros = acros;
	}

	public String getNew_Field_Name() {
		return new_Field_Name;
	}

	public void setNew_Field_Name(String new_Field_Name) {
		this.new_Field_Name = new_Field_Name;
	}


	public AcronymExpansionCommand() {
		super();
	}
	
	
	@Override
	public String toString() {
		return "AcronymExpansionCommand [fieldName=" + fieldName + ", acros=" + acros + ", new_Field_Name="
				+ new_Field_Name + "]";
	}
	@Override
	
	public List<Map<String, String>> execute(List<Map<String, String>> dataList) {
		Map<String, String> acronyms_ToMatch = this.getAcros();
		List<Map<String, String>> datanew_map = new ArrayList<Map<String, String>>();
		for (int i = 0; i < dataList.size(); i++) {
			
			Map<String, String> tmpMap = new HashMap<String, String>();
			List<String> matchedAcros = new ArrayList<String> ();
			Map<String, String> toProcess = dataList.get(i);
			for (Map.Entry<String, String> entry : toProcess.entrySet()) {
				if (fieldName.contains(entry.getKey())) {
					String pattern = null;
					String matchString = entry.getValue();

					for (Map.Entry<String, String> m1 : acronyms_ToMatch.entrySet()) {

						pattern = m1.getKey();
						if ((matchString.contains(pattern)) == true)
					    matchedAcros.add(pattern);		
					}
					tmpMap.put(entry.getKey(), entry.getValue());
					if (matchedAcros.isEmpty() == false) {
					List<String> matchedAcrosString = new ArrayList<String> ();
					for(int m = 0 ; m< matchedAcros.size(); m++) {
						 matchedAcrosString.add(matchedAcros.get(m)+ " " + " " + acronyms_ToMatch.get(pattern));
					}
					String join = matchedAcrosString.stream()
							      .collect(Collectors.joining(", "));
                    tmpMap.put(new_Field_Name, join);
					}
				} else {
					tmpMap.put(entry.getKey(), entry.getValue());
				}

			}
			datanew_map.add(tmpMap);
		}
		return datanew_map;

	}

}
